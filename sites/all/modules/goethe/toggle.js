jQuery(document).ready(function() {
    var domain_all_off = true;
    jQuery('#domain_toggle').click(function() {
        domain_all_off ^= true;
        jQuery('.form-item-domains input').attr('checked', (domain_all_off ? false : 'checked'));
    });
});