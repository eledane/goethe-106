<?php if (!empty($q)): ?>
    <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
    ?>
<?php endif; ?>
<?php if( empty($_GET['date'])) {
   $start_date=next_exam_time();

} else {
    $start_date=$_GET['date'];
}
?>

    <div class="city">
        <div class="input selectbox">
            <?php print $widgets['filter-gid']->widget;?>
        </div>
</div>
    <?php print $widgets['filter-field_subtitle_value_selective']->widget;?>
    <div style="display: none">
        <input type="hidden" name='date' value="<?php print $start_date ?>" >
        <?php print $button; ?>
    </div>

