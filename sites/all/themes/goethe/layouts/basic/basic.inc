<?php
// Plugin definition
$plugin = array(
    'title' => t('Standard Page'),
    'category' => t('Custom'),
    'icon' => 'basic.png',
    'theme' => 'basic',
    'css' => 'basic.css',
    'regions' => array(
        'header' => t('Header'),
        'banner' => t('Banner'),
        'content' => t('Content'),
        'right' => t('Right'),
        'above' => t('Above Content'),
        'below' => t('Below'),
        'footer' => t('Footer'),
    ),
);
