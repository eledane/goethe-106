<?php
// Plugin definition
$plugin = array(
    'title' => t('Single Column'),
    'category' => t('Custom'),
    'icon' => 'single.png',
    'theme' => 'single',
    'css' => 'single.css',
    'regions' => array(
        'header' => t('Header'),
        'banner' => t('Banner'),
        'content' => t('Content'),
        'above' => t('Above Content'),
        'below' => t('Below'),
        'footer' => t('Footer'),
    ),
);
