<?php
// Plugin definition
$plugin = array(
    'title' => t('City Header'),
    'category' => t('Custom'),
    'icon' => 'city_header.png',
    'theme' => 'city_header',
    'css' => 'city_header.css',
    'regions' => array(
         'header' => t('Header'),
        'content' => t('Content'),
    ),
);
