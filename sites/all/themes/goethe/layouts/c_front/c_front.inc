<?php
// Plugin definition
$plugin = array(
    'title' => t('City Front Page'),
    'category' => t('Custom'),
    'icon' => 'c_front.png',
    'theme' => 'c-front',
    'css' => 'c_front.css',
    'regions' => array(
        'header' => t('Header'),
        'banner' => t('Banner'),
        'content' => t('Content'),
        'content_top' => t('Top Content'),
        'right' => t('Right'),
        'above' => t('Above Content'),
        'below' => t('Below Content'),
        'footer' => t('Footer'),
    ),
);
