<?php $domain = domain_get_domain(); ?>
<div id="container">

    <div id="header">
    
        <?php print $content['header']; ?>   
    </div><!--#header-->
    <div class="clear"></div>

    <div id="main">

        <div class="homebox">
            <div class="homeBanner">
                <div class="bg"><img src="/sites/all/themes/goethe/images/guten/homebanner.jpg" alt=""></div>
                <div class="contacts">
                    <div class="cont">
                        <h1>
                            <?php //print t('Master German with !gutentag and start making leaps in your career!', array('!gutentag' => '<strong>' . t('Guten Tag !city', array('!city' => $domain['sitename'])) . '</strong>')); 
                                print(t('Learn German. Get a Taste of Germany'));
                            ?>
                        </h1>
                    </div>
                </div>
                <div class="startNow"><a href="#courses" class="up"><?php print t('start now'); ?> <img src="/sites/all/themes/goethe/images/ico/ico-start.png" alt=""></a></div>
                <div class="btn"><a href="#about-us" class="up"></a></div>
            </div>
        </div>

        <?php print $content['above']; ?>

        <div class="main_box">
            <div class="kursen" id="courses">
                <div class="kTitle"><h2><?php print t('Courses'); ?></h2></div>

                <?php print $content['content_top']; ?>
                <div class="left">
                    <?php print $content['content']; ?>
                </div>
                <div class="right">
                    <?php print $content['right']; ?>
                </div>
            </div>
            
        </div>

        <?php print $content['below']; ?>
    </div><!--#main-->


    <div class="clear"></div>
    <div id="footer">
        <div class="f_box">
            <?php print $content['footer']; ?>
        </div>
    </div>

    <div id="backTop"><a href="#"></a></div>
</div>