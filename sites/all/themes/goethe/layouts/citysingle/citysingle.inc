<?php
// Plugin definition
$plugin = array(
    'title' => t('City Single Column'),
    'category' => t('Custom'),
    'icon' => 'citysingle.png',
    'theme' => 'citysingle',
    'css' => 'citysingle.css',
    'regions' => array(
        'header' => t('Header'),
        'banner' => t('Banner'),
        'content' => t('Content'),
        'above' => t('Above Content'),
        'below' => t('Below'),
        'footer' => t('Footer'),
    ),
);
