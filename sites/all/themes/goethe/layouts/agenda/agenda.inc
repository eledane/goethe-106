<?php
// Plugin definition
$plugin = array(
    'title' => t('Agenda Page'),
    'category' => t('Custom'),
    'icon' => 'agenda.png',
    'theme' => 'agenda',
    'css' => 'agenda.
    css',
    'regions' => array(
        'header' => t('Header'),
        'banner' => t('Banner'),
        'content' => t('Content'),
        'above' => t('Above Content'),
        'below' => t('Below'),
        'footer' => t('Footer'),
    ),
);
