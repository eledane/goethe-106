<?php $domain = domain_get_domain(); ?>
<div class="cicyheader">
<div class="h_box">
	<div class="logo">
		<a href="<?php print url('<front>'); ?>"><img
			src="/sites/all/themes/goethe/images/city_logo/logo-cn-<?php print strtolower($domain['sitename']);?>.png"
			alt="Goethe"> </a>
	</div>
	<div class="h_right">
	<?php print $content['header']; ?>
	</div>
	<div id="menu">
	<?php print $content['content']; ?>
	</div>
	
	<script type="text/javascript">
						jQuery(document).ready(function(){

							jQuery(".cicyheader .search a").click(function(){
								jQuery(".cicyheader .search .searchbox").slideToggle();
							});
						});
                    </script>
	
</div>
</div>

