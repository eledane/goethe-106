<?php
// Plugin definition
$plugin = array(
    'title' => t('Header'),
    'category' => t('Custom'),
    'icon' => 'header.png',
    'theme' => 'header',
    'css' => 'header.css',
    'regions' => array(
          'header' => t('Header'),
        'content' => t('Content'),
    ),
);
