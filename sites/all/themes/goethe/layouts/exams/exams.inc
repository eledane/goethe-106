<?php
// Plugin definition
$plugin = array(
    'title' => t('Exams'),
    'category' => t('Custom'),
    'icon' => 'exams.png',
    'theme' => 'exams',
    'css' => 'exams.css',
    'regions' => array(
        'header' => t('Header'),
        'banner' => t('Banner'),
        'content' => t('Content'),
        'right' => t('Right'),
        'above' => t('Above Content'),
        'below' => t('Below'),
        'footer' => t('Footer'),
    ),
);
