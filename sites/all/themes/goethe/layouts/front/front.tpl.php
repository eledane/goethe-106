<div id="container">

    <div id="header">    
                <?php print $content['header']; ?>     
     </div><!--#header <?php echo __FILE__;?> -->
    <div class="clear"></div>

    <div id="main">
        <?php print $content['content']; ?>
    </div><!--#main-->


    <div class="clear"></div>
    <div id="footer" <?php if($_GET['q'] !== '<front>') print 'style="margin-top:0"'; ?>>
        <div class="f_box">
            <?php print $content['footer']; ?>
        </div>
    </div>

    <div id="backTop"><a href="#"></a></div>
</div>