<?php
// Plugin definition
$plugin = array(
    'title' => t('Front'),
    'category' => t('Custom'),
    'icon' => 'front.png',
    'theme' => 'front',
    'css' => 'front.css',
    'regions' => array(
        'header' => t('Header'),
        'content' => t('Content'),
        'footer' => t('Footer'),
    ),
);
