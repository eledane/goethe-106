<div id="container">

    <div id="header">
                <?php print $content['header']; ?>
        </div>
    </div><!--#header <?php echo __FILE__;?> -->
    <div class="clear"></div>

    <div id="main">

        <div class="banner">
            <?php print $content['banner']; ?>
        </div>


        <div class="main_box">
            <div class="<?php print $css_id; ?>">
                <div class="left appThankyou">
                    <?php print $content['above']; ?>
                    <?php print $content['content']; ?>
                </div>
                    
                <div class="clearfix"></div>
            </div>
        </div>

        <?php print $content['below']; ?>

    </div><!--#main-->

    <div class="clear"></div>
    <div id="footer">
        <div class="f_box">
            <?php print $content['footer']; ?>
        </div>
    </div>

    <div id="backTop"><a href="#"></a></div>

</div>