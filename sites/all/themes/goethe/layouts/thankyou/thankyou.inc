<?php
// Plugin definition
$plugin = array(
    'title' => t('Thank you Page'),
    'category' => t('Custom'),
    'icon' => 'thankyou.png',
    'theme' => 'thankyou',
    'css' => 'thankyou.css',
    'regions' => array(
        'header' => t('Header'),
        'banner' => t('Banner'),
        'content' => t('Content'),
        'above' => t('Above Content'),
        'below' => t('Below'),
        'footer' => t('Footer'),
    ),
);
