(function($) {
	/*input*/
	$("span.inputText").parent().css({position:"relative",zIndex:1});
	$("input").live("keyup focus", function(){
		if($(this).val()){
			$(this).next("span.inputText").hide();
		}else{
			$(this).next("span.inputText").show().css({opacity:0});
		}
	}).live("blur", function(){
		if (!$(this).val()) {
			$(this).next("span.inputText").show().css({opacity:1});
		}
	});
  $("textarea,input").live("keyup focus", function(){
		if($(this).val()){
			$(this).parent().next("span.inputText").hide();
		}else{
			$(this).parent().next("span.inputText").show().css({opacity:0});
		}
	}).live("blur", function(){
		if (!$(this).val()) {
			$(this).parent().next("span.inputText").show().css({opacity:1});
		}
	});
	$("span.inputText").live("click",function(){
		$(this).css({opacity:0});
		$(this).parent().find("input,textarea").focus();
	});

  /* move messages */
  $(document).ready(function() {
    $('.messages').prependTo($('.main_box')).fadeIn();

    $("input").each(function(){
      if ($(this).val().length > 0) {
        $(this).next("span.inputText").hide();
      }
    });
    $("textarea").each(function(){
      if ($(this).val().length > 0) {
        $(this).parent().next("span.inputText").hide();
      }
    });
    $("input").each(function(){
      if ($(this).val().length > 0) {
        $(this).parent().next("span.inputText").hide();
      }
    });
  });

})(jQuery);

