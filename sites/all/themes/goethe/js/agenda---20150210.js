var settings = {};

(function ($) {
  function filterExams() {
    settings['levels'] = [];
    $('#exam_slide td div').hide();
    $('#exam_slide .checkboxListFilter li').each(function(i, el) {
      if($(el).hasClass("selected")) {
        var id = $(el).attr('id');
        $('#exam_slide td.' + id +' div').show();
        settings['levels'].push(id);
      }
    });

    if($('#exam_slide .checkboxListFilter li.selected').length == 0) {
      $('#exam_slide td div').show(); // show all if nothing selected
    }

    // hide all cities not currently selected
    var city_name = $('#exam_slide .selectbox input').val();
    var domain_class = $('#exam_slide .selectbox li:contains('+city_name+')').attr('id');
    if(domain_class) {
        if(domain_class != 'domain-all') {
            jQuery('#exam_slide td:not(.'+domain_class+') div').hide()
        }
        settings['domain'] = domain_class;
    }
  }

  function preparePager() {
    if($('#realpage .pager-previous a').length == 0){
        $('#presspager .prev').hide();
      }

      if($('#realpage .pager-next a').length == 0){
        $('#presspager .next').hide();
      }
  }

  function resetSettings() {
    if(settings['levels'].length) {

      $.each(settings['levels'], function(i, el) {
        $('#'+el).click();
      });
    }
    if(settings['domain']) {
      $('#'+settings['domain']).click();
    }
  }

  function startLoad() {
    if($('#exam_slide').length == 1) {
      $('.view-id-exams').css('opacity', 0.5);
    } else {
      $('.view-id-press .press').css('opacity', 0.5);
    }
  }

  function endLoad() {
    if($('#exam_slide').length == 1) {
      resetSettings();
      $('.view-id-exams').css('opacity', 1);
    } else {
      $('.view-id-press .press').css('opacity', 1);
      preparePager();
    }
  }

  // modify ajax success function to call an event
  Drupal.ajax.prototype.success = function (response, status) {
    if (this.progress.element) { $(this.progress.element).remove(); }
    if (this.progress.object) { this.progress.object.stopMonitoring(); }
    $(this.element).removeClass('progress-disabled').removeAttr('disabled');
    Drupal.freezeHeight();
    for (var i in response) {
      if (response.hasOwnProperty(i) && response[i]['command'] && this.commands[response[i]['command']]) {
        this.commands[response[i]['command']](this, response[i], status);
      }
    }
    if (this.form) {
      var settings = this.settings || Drupal.settings;
      Drupal.attachBehaviors(this.form, settings);
    }
    Drupal.unfreezeHeight();
    this.settings = null;

    endLoad();
  };

  $(document).ready(function() {
    if($('#exam_slide').length == 1) {
      // ====== exam page ======

      // back-fowards paging
      $('#exam_slide .prev').live('click', function(e) {
        e.preventDefault();
        $('#exam_slide .date-prev a').click();
        startLoad();
      });
      $('#exam_slide .next').live('click', function(e) {
        e.preventDefault();
        $('#exam_slide .date-next a').click();
        startLoad();
      });

      // filtering
      $('#exam_slide .checkboxListFilter li').live('click', function(e) {
        $(this).toggleClass('selected');
        filterExams();
      });

      $('#exam_slide .selectbox li').live('click', function(e) {
        filterExams();
      });
    }

    if($('#presspager').length == 1) {
      // ====== press page ======

      // back-fowards paging
      $('#presspager .prev a').live('click', function(e) {
        e.preventDefault();
        $('#realpage .pager-previous  a').click();
        startLoad();
      });
      $('#presspager .next a').live('click', function(e) {
        e.preventDefault();
        $('#realpage .pager-next a').click();
        startLoad();
      });

      preparePager();
    }

  });
})(jQuery);
