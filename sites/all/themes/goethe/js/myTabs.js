(function($) {
$(document).ready(function(){
    $(".myTabsBtn .rightArrow").live('click',function(){
        var contentContainer = $(this).closest('.myTabs').find('.myTabsCon');
        var total = contentContainer.find('.oneCon').length;
        var currentPage = parseInt($(this).parent().find('span .curSpan').text());
        if(currentPage >= total) return;

        $(this).parent().find('span .curSpan').text(++currentPage);
        contentContainer.find('.oneCon').hide();
        contentContainer.find('.oneCon').eq(--currentPage).show();
    });
    $(".myTabsBtn .leftArrow").live('click',function(){
        var contentContainer = $(this).closest('.myTabs').find('.myTabsCon');
        var total = contentContainer.find('.oneCon').length;
        var currentPage = parseInt($(this).parent().find('span .curSpan').text());
        if(currentPage == 1) return;

        $(this).parent().find('span .curSpan').text(--currentPage);
        contentContainer.find('.oneCon').hide();
        contentContainer.find('.oneCon').eq(--currentPage).show();
    });
})
})(jQuery);