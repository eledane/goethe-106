﻿(function($) {
  $(document).ready(function(){

    // back top
    $(window).scroll(function() {
      var scrolltop = $(window).scrollTop();
      var browserheight = $(window).height();
      if(scrolltop > browserheight){
        $("#backTop").show();
      }else{
        $("#backTop").hide();
      }

    });
	
    //锚点滑动
    $('a.up[href*=#]').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var $target = $(this.hash);
        $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
        if ($target.length) {
          var targetOffset = $target.offset().top;
          $('html,body').animate({
                scrollTop: targetOffset
              },
              500);
          return false;
        }
      }
    });


    /*form*/
    $(".selectbox input,.sortby input").live('click', function(i){
      $(this).next("ul").slideDown();
    });
    $(".selectbox ul li").live('click', function(e){
      e.preventDefault();
      var theval = $(this).find("a").html();
      var key = $(this).parents('.selectbox').find("option:contains(" + theval + ")").attr('value');

      $(this).parents(".selectbox").find("input").attr({"value":theval});
      $(this).parents(".selectbox").find("select").attr({"value":key});
      $(this).parents(".selectbox").find("ul").slideUp(100);
    });
    $(".radioList li").click(function(){
      $(this).addClass("selected").siblings("li").removeClass("selected");
    });
    /*$(".checkboxList li,.iagree span").click(function(){
      $(this).toggleClass("selected");
    });*/

    $('.checkboxList li').click(function() {
        var val = $(this).text();
        $(this).parent().find('option').attr('selected', '');
        $(this).parent().find('option:contains('+val+')').attr('selected', 'selected');
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
    });

      $('.checkboxSingle li').click(function() {
          $(this).parent().find('input').click();
          $(this).toggleClass('selected');
      });


    //
    var artNum = $(".homeBanner .links ul li").length;
    for (var i=0;i<artNum;i++){
      if((i+1)%3 == 0){
        $('.homeBanner .links ul li:eq('+i+')').css('margin-right','0');
      }
    }



    // exam booking dropdown
    $(".aTitle .cont").click(function(){
      $(this).next(".aList").slideToggle(300);
      return false;
    });

    $(".aTitle .aList ul li a").click(function(){
      var thea = $(this).html();
      $(this).parents(".aTitle").find("h3").html(thea);
      $(this).parents(".aTitle").find(".aList").hide();
      //return false;
    });


    $(".sortby ul li").click(function(){
      var theval = $(this).find("a").html();
      $(this).parents(".sortby").find("input").attr({"value":theval});
      $(this).parents(".sortby").find("ul").slideUp(100);
    });

    $(".find_main .tag ul li").each(function(i){
      $(this).click(function(){
        $(this).addClass("active").siblings("li").removeClass("active");
        $(".find_main .cont .list").eq(i).show().siblings(".list").hide();
      });
    });

    //#lemumgebung .list

    var browserwidth = $(window).width();
    $(".slides_container div.list").css({width:browserwidth})
    if(browserwidth<1440){
      $(".slides_container div.list img").css({width:"auto"});
    }else{
      $(".slides_container div.list img").css({width:"100%"});
    }

    $(window).resize(function(){
     $(".homeBanner .bg img, .home_list .bg img").fullBg();
      slidesWidth();
    });

    $("#lemumgebung .list").css({width:browserwidth});
    $(window).resize(function(){
      var browserwidth = $(window).width();
      $("#lemumgebung .list").css({width:browserwidth});
    });

    //menu
    $("#menu ul li").hover(function(){
      $(this).find("a").eq(0).addClass("selected");
      $(this).find(".menu_up").slideDown(300);

    },function(){
      $(this).find("a").eq(0).removeClass("selected");
      $(this).find(".menu_up").slideUp(50);
    });

    $('.weixin a').click(function() {
        $('.weixin-content').toggle();
    });

    $('body').click(function(){
      $('.selectbox ul').slideUp(100);
    });

    /* $(".selectbox input,.sortby input").blur(function(i){
      $(this).next("ul").slideUp(100);
    }); */

    // homebanner resizing
    $(window).resize(function() {
      resizeBanner();
    });
    resizeBanner();

  });

  function slidesWidth() {
    var browserwidth = $(window).width();
    $(".slides_container div.list").css({width:browserwidth})
    if(browserwidth<1440){
      $(".slides_container div.list img").css({width:"auto"});
    }else{
      $(".slides_container div.list img").css({width:"100%"});
    }
  }

  function resizeBanner() {
    // max: 1440 x 900
    var height = $(window).height();
    var width = $(window).width();

    $('.homeBanner, .homeBanner .bg, .homeBanner .bg img').height(height);
    $('.home_list .bg, .home_list .photo, .home_list .bg img').height(height);

    $('.home_list .contacts').css('top', height/2 - 100);

    if(width < 1440) {
      $('.homeBanner .bg img, .home_list .bg img').width('1440px');
    } else {
      if(height > 900 && width < (1440*height) / 900) {
        $('.homeBanner .bg img, .home_list .bg img').width('auto').height('100%');
      } else {
        $('.homeBanner .bg img, .home_list .bg img').width('100%').height('auto');
      }
    }

    if($('.globalLanding').length) {
      if(height < 830) {
        // small style front page
        $('.homeBanner .cont h1').css({'padding-top': '10px', 'line-height':'50px', 'font-size': '35px'});
        $('.homeBanner .cont h1 strong').css('font-size', '50px');
        $('.homeBanner .cont .links').css('margin-top', '25px');
      } else {
        $('.homeBanner .cont h1').css({'padding-top': '90px', 'line-height':'60px', 'font-size': '45px'});
        $('.homeBanner .cont h1 strong').css('font-size', '60px');
        $('.homeBanner .cont .links').css('margin-top', '75px');
      }
    } else {
        if(height < 660) {
            $('.homeBanner .contacts').css('margin-top', '200px');
        } else if(height < 730) {
            $('.homeBanner .contacts').css('margin-top', '280px');
        } else {
            $('.homeBanner .contacts').css('margin-top', '345px');
        }
    }
  }

})(jQuery);
