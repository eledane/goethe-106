<?php
//print_r($form);
$form['submit']['#value'] = '';
$form['body_value']['#title'] = 'Search';
$form['body_value']['#suffix'] = "<span class='inputText'>". t('Search')."</span>";

?> 


<div class="search"><a href="#"></a>
                	
                    <div class="searchbox">
                    	<div class="input"><?php  print render($form['body_value']);?></div>
                        <div class="btn"><?php   print render($form['submit']); ?></div>
                    </div>               
                    <script type="text/javascript">
						jQuery(document).ready(function(){

							jQuery("#menu .search a").click(function(){
								jQuery("#menu .search .searchbox").slideToggle();
							});
						});
                    </script>
                </div>
                
                
 <?php
drupal_render_children($form);