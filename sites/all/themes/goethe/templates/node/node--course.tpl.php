<div class="title"><h2><?php print $node->title?></h2><div class="back"><a href="<?php print url('courses'); ?>/<?php print $content['field_course_type']['#items'][0]['tid'];?>">&lt;  <?php print t('Back'); ?></a></div></div>
<div class="list text">
    <div class="left">
        <dl>
            <dt><?php print t('Duration');?>: </dt>
            <dd><?php print render($content['field_duration'])?></dd>
        </dl>
        <dl>
            <dt><?php print t('Progress');?>:</dt>
            <dd><?php print render($content['field_progress']);?></dd>
        </dl>
        
        <dl>
            <dt><?php print t('Level:'); ?></dt>
            <dd>
                <?php $i = 1; $lines = explode('<br/>', render($content['field_level'])); foreach($lines as $line): ?>
                <?php if($line[0] == '*') { break; } ?>
                <?php $short_lines[] = $line; $i++; ?>
                <?php endforeach;?>
                <?php print implode(', ', $short_lines); ?>
            </dd>
        </dl>

        <dl>
            <dt><?php print t('Description:'); ?></dt><dd>
            <?php for($j = $i-1; $j < count($lines); $j++): ?>
                <?php print $lines[$j];?><br/>
            <?php endfor; ?>
            </dd>
        </dl>
        
        <dl>
            <dt><?php print t('Group Size');?>:</dt>
            <dd><?php print render($content['field_group_size']); ?></dd>
        </dl>

    </div>
    <div class="right">
        <div class="price"><span><?php print render($content['field_price']);?> </span><?php print t('RMB');?></div>
        <p><?php print render($content['field_price_terms']);?></p>
        <div class="btn"><a href="<?php print url('book-course/'.$node->nid);?>" class="btn_anmeldung"><?php print t('Register'); ?></a></div>
    </div>
    <div class="clearfix"></div>
    <div class="kurse_detail">
        <p><strong><?php print t('Brief');?></strong>
            <?php print render($content['body']);?>
    </div>
</div><!--list-->
