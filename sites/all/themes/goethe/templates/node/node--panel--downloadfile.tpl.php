<?php
// get node from other domain if available
list($node, $content) = domain_translate($node, $content);
?>
<div class="info">
    <p><?php print render($content['body']); ?></p>
    <?php 
    	if(isset($node->field_file[LANGUAGE_NONE][0]['fid'])) {
    ?>
    <div class="btn">
        <a href="<?php print render($content['field_file']); ?>" class="btn_down">
            <img src="/sites/all/themes/goethe/images/ico/ico-down.png" alt=""> <?php print render($content['field_link_title']); ?>
        </a>
    </div>
    <?php }?>
</div>