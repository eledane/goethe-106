<?php
$times = $content['field_traninging_time']['#items'][0];
$endtime = ($times['value2'] ? $times['value2'] : $times['value']);

$past_url = url('training', array('query' => array('v' => 'pst')));
$next_url = url('training');

$past = false;
if(time() > $endtime) {
    $past = true;
    $back_url = $past_url;
} else {
    $back_url = $next_url;
}
?>
<div class="list">
    <div class="title">
        <h3><a href="<?php print $next_url; ?>" <?php print ($past ? '' : 'class="active"'); ?>><?php print t('Upcoming Training'); ?></a></h3>
        <h3 class="last"><a href="<?php print $past_url; ?>" <?php print (!$past ? '' : 'class="active"'); ?>><?php print t('Past training'); ?></a></h3>
        <div class="back"><a href="<?php print $back_url; ?>">&lt;  <?php print t('Back'); ?></a></div>
    </div>
    <dl class="detail last">
        <dt><?php print render($content['field_picture']); ?></dt>
        <dd>
            <h4><?php print $node->title; ?></h4>
            <div class="timeAddress">
                <div class="time"><img src="/sites/all/themes/goethe/images/ico/ico-time.png" alt=""> <?php print render($content['field_traninging_time']); ?></div>
                <div class="address"><img src="/sites/all/themes/goethe/images/ico/ico-address.png" alt=""> <?php print render($content['field_address_at']); ?></div>
            </div>
            <div class="text">
               <?php print render($content['body']); ?>
            </div>
            <h4 class="t20"><?php print t('Training documents'); ?></h4>
            <div class="btn">
                <a href="#TB_inline?height=150&amp;width=450&amp;inlineId=enterPass" class="thickbox btn_down"><img alt="" src="/sites/all/themes/goethe/images/ico/ico-down2.png"> <?php print t('Download The PDF'); ?></a>
            </div>
        </dd>
    </dl>
</div>
<div id="enterPass" style="display:none;">

    <div class="downPassword">
        <h5><?php print t('Enter your teacher password to download'); ?></h5>
        <div class="input">
            <?php print drupal_render(drupal_get_form('teacher_download_form', $node)); ?>
        </div>
    </div>

</div>