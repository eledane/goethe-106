<?php if(!@$content['field_level'] && !@$content['field_duration']) { $short = true; } else { $short = false; } ?>

<div class="title"><h2><?php print t('Dear Madam or Sir, we have received your information and will contact you directly within the next two working days, in order to facilitate further steps in the registration process. In case you do not hear from us within two days, please get in touch with us through phone or email.'); ?></h2></div>
<div class="list">
    <div class="left" <?php if($short) { print ' style="width:100%"'; } ?>>
        <h3><a href="<?php print url('node/'. $node->nid); ?>" class="green"><?php print $node->title; ?></a></h3>
        <?php if(!$short): ?>
        <dl>
            <dt><?php print t('Duration');?>: </dt>
            <dd><?php print render($content['field_duration'])?></dd>
        </dl>
        <dl>
            <dt><?php print t('Progress');?>:</dt>
            <dd><?php print render($content['field_progress']);?></dd>
        </dl>
        <dl>
            <dt><?php print t('Level:'); ?></dt>
            <dd>
                <?php $i = 1; $lines = explode('<br/>', render($content['field_level'])); foreach($lines as $line): ?>
                <?php if($line[0] == '*') { break; } ?>
                <?php $short_lines[] = $line; $i++; ?>
                <?php endforeach;?>
                <?php print implode(', ', $short_lines); ?>
            </dd>
        </dl>

        <dl>
            <dt><?php print t('Description:'); ?></dt><dd>
            <?php for($j = $i-1; $j < count($lines); $j++): ?>
                <?php print $lines[$j];?><br/>
            <?php endfor; ?>
            </dd>
        </dl>
        <dl>
            <dt><?php print t('Group Size');?>:</dt>
            <dd><?php print render($content['field_group_size']); ?></dd>
        </dl>
        <?php else: ?>
        <dl>
            <dd><?php print render($content['body']); ?></dd>
        </dl>
        <?php endif; ?>
    </div>
    <div class="right">
        <?php if(!$short): ?>
        <div class="price"><span><?php print render($content['field_price']);?> </span><?php print t('RMB');?></div>
        <p><?php print render($content['field_price_terms']);?></p>
        <?php endif; ?>
    </div>
</div><!--list-->
