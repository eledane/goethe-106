<?php $tags=explode('<br/>',render ($content['field_blog_tags']));
$taghtml=implode(",",$tags);

?>
<div class="title">
    <h2><?php print $node->title; ?></h2>
    <div class="back"><a href="<?php print url('blog'); ?>">< <?php print t('Back');?> </a></div>
</div>
<div class="kultur_cont">
    <p><?php print t('Author');?>: <?php print $node->name;?>       <?php print t("Date");?>: <?php print $date;?>      <?php print t("Tags");?>: <?php print $taghtml;?></p>
    <?php print render($content['body']);?>
</div>
                        