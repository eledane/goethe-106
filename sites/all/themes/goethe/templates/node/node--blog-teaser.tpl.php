<dt><a href="<?php print url('node/'.$node->nid)?>"><?php print render($content['field_related_picture']);?></a></dt>
<dd>
    <h3><?php print $node->title?></h3>
    <p><?php print render($content['body'])?></p>
    <div class="btn">
        <a href="<?php print url('node/'.$node->nid)?>" class="more">More <img src="/sites/all/themes/goethe/images/ico/ico-more.png" alt="" /></a>
    </div>
</dd>
