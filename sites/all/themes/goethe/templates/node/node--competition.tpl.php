<?php
$completed = (render($content['field_competition_status']) == 'Completed');
$complete_url = url('competitions', array('query' => array('v' => 'pst')));
$upcoming_url = url('competitions');
$back_url = ($completed ? $complete_url : $upcoming_url);
?>

<div class="list">
    <div class="title">
        <h3><a href="<?php print $upcoming_url; ?>" <?php print (!$completed ? 'class="active"' : ''); ?>><?php print t('Upcoming Competitition'); ?></a></h3>
        <h3 class="last"><a href="<?php print $complete_url; ?>" <?php print ($completed ? 'class="active"' : ''); ?>><?php print t('Past Competition'); ?></a></h3>
        <div class="back"><a href="<?php print $back_url; ?>">&lt;  <?php print t('Back'); ?></a></div>
    </div>
                    	
    <dl class="last">
        <dd>
            <h4><?php print $node->title; ?></h4>
            <div class="timeAddress">
                <div class="time"><img src="/sites/all/themes/goethe/images/ico/ico-time.png" alt=""> <?php print render($content['field_competition_date']); ?></div>
                <div class="address"><img src="/sites/all/themes/goethe/images/ico/ico-address.png" alt=""> <?php print render($content['field_address_at']); ?></div>
            </div>
            <div class="text">
                <?php print render($content['body']); ?>
            </div>
            <div class="btn">
                <a href="<?php print render($content['field_flyer']); ?>" class="btn_down"><img src="/sites/all/themes/goethe/images/ico/ico-down2.png" alt=""> <?php print t('Download Flyer'); ?></a>
            </div>
                                
        </dd>
        <?php if($completed): ?>
        <dd class="right">
            <div class="title">
                <h4><?php print t('The Winner / Group'); ?></h4>
                <h5><?php print render($content['field_winner_name']); ?></h5>
            </div>
            <div class="photo">
                <?php print render($content['field_winner_picture']); ?>
            </div>
        </dd>
        <?php endif; ?>
    </dl>
</div>
<?php if(count(@$content['field_competition_pictures']['#items'])): ?>
<div class="other_list">
    <h2><?php print t('Pictures'); ?></h2>
    <ul>
        <?php foreach(explode('<br/>', render($content['field_competition_pictures'])) as $pic): ?>
        <li><?php print $pic; ?></li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>

<?php if(count(@$content['field_competition_videos']['#items'])): ?>
<div class="other_list">
    <h2><?php print t('Videos'); ?></h2>
    <ul>
        <?php foreach(explode('<br/>', render($content['field_competition_videos'])) as $video): ?>
        <li><iframe height=200 width=300 src="http://player.youku.com/embed/<?php print $video; ?>" frameborder=0 allowfullscreen></iframe></li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>