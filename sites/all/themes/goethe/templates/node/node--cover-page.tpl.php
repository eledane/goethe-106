<?php
list($node, $content) = domain_translate($node, $content);
$next = render($content['field_next_pane_anchor']);
$more = render($content['field_read_more_link']);
$more_link = ($more ? ' ' . l(t('Read more >'), $more, array('attributes' => array('class' => array('green')))) : '');

$more_2 = render($content['field_read_more_button_text']);
$more_2 = explode('|', $more_2);
$more_button_text = $more_2[0];
$more_button_link = $more_2[1];
?>
<div class="home_list" id="<?php print render($content['field_page_id']); ?>">
    <div class="photo" id="node-<?php print $node->nid; ?>">
        <div class="bg"><?php print render($content['field_cover_picture']); ?></div>
    </div>
    <div class="contacts">
        <div class="cont">
            <h2 style="font-size:35px;"><?php print $node->title; ?></h2>
            <?php print append_to_last_p($more_link, render($content['body'])); ?>
            <?php if(count(@$content['field_read_more_button_text']['#items'])): ?>
            <div class="btn_green"><a href="<?php print url($more_button_link); ?>"><?php print $more_button_text; ?></a></div>
            <?php endif; ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <?php if($next): ?>
    <div class="btn"><a href="#<?php print $next; ?>" class="up"></a></div>
    <?php endif; ?>
</div>