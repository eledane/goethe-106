<?php
list($node, $content) = domain_translate($node, $content);
$body=render($content['body']);?>

<div class="rightlist">
    <div class="title"><h2><?php print $node->title; ?></h2></div>
    
    <div class="cont">
        <div class="terms">
            <?php print $body; ?>
        </div>
    </div>
</div>
