<?php
$domains = load_all_domains();
$next = render($content['field_next_pane_anchor']);

$existing = array(
    // '1' => 'http://www.goethe-slz.cq.cn/',
    // '5' => 'http://www.qdgoethe.net/',
    // '6' => 'http://www.goethe-slz.js.cn/',
    // '2' => 'http://www.goethe-slz.sh.cn/',
    // '7' => 'http://www.goethe-slz.sn.cn/',
);
?>
<div class="homebox" id="<?php echo "node-".$node->nid;?>">
    <div class="homeBanner globalLanding">
        <div class="bg"><?php print render($content['field_cover_picture']); ?></div>
        <div class="cont">
            <h1><?php print render($content['body']); ?></h1>
            <div class="links">
                <ul>
                    <?php $iter = 1; foreach($domains as $domain): ?>
                        <li<?php print (($iter++ % 3) == 0 ? ' style="margin-right:0px;"' : ''); ?>>
                            <?php if(isset($existing[$domain['domain_id']])): ?>
                                <a target="_blank" href="<?php print $existing[$domain['domain_id']]; ?>">
                            <?php else: ?>
                            <?php
                              $uri = domain_get_uri($domain); 
                              
                               if($uri == 'http://wuhan.goetheslz.com/' || $uri == 'http://wuhan.goetheslz.com/de'){
                                 $uri = 'https://www.goethe.de/ins/cn/de/sta/pek/ueb/imp/unterzeichnungs.html';
                                } elseif ($uri == 'http://wuhan.goetheslz.com/zh'){
                                 $uri = 'https://www.goethe.de/ins/cn/zh/sta/pek/ueb/imp/unterzeichnungs.html';
                                } else {}
                             
                               ?>
                                <a href="<?php print $uri; ?>">
                            <?php endif; ?>
                            <span><?php print t('Goethe-Sprachlernzentrum !city', array('!city' => '<strong>'.t($domain['sitename']).'</strong>')); ?></span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php if($next): ?>
        <div class="btn"><a href="#<?php print $next; ?>" class="up"></a></div>
        <?php endif; ?>
    </div>
</div>
