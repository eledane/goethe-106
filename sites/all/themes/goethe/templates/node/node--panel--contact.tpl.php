<div class="title"><h2><?php print $node->title; ?></h2></div>
<ul>
    <li>
        <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-tel.gif" alt=""></span>
        <span class="text"><?php print render($content['field_telephone']); ?></span>
    </li>
    <li>
        <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-fax.gif" alt=""></span>
        <span class="text"><?php print render($content['field_fax']); ?></span>
    </li>
    <li>
        <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-email.gif" alt=""></span>
        <span class="text"><?php print render($content['field_e_mail']); ?></span>
    </li>
    <li>
        <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-time.gif" alt=""></span>
        <span class="text"><?php print render($content['field_opening_hours']); ?></span>
    </li>
</ul>
<div class="followus-holder" style="display:none">
        <ul>
            <li class="weixin"><a></a><div class="weixin-content"><?php print render($content['field_weixin']); ?></div></li>
            <li class="weibo"><a target="_blank" href="<?php print render($content['field_weibo']); ?>"></a></li>
        </ul>
    </div>
<p><strong><?php print t('Transportation'); ?>: </strong><?php print render($content['field_transport']); ?></p>
<p><strong><?php print t('Address'); ?>: </strong><?php print render($content['field_address']); ?></p>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.contactfrom .followus').html(jQuery('.followus-holder').html());
    });
</script>