<?php
// get node from other domain if available
list($node, $content) = domain_translate($node, $content);
?>
<?php $body=render($content['body']);?>
<?php if($body==''):?>
<div class="right">
    <?php endif; ?>
    <div class="rightlist">
        <div class="title"><h2><?php print $node->title; ?></h2></div>
        <?php if($body){?>
        <div class="cont paddedp" style="padding:19px;">
            <?php print $body; ?>
      
        </div>
        <?php }else{?>
        <div class="cont">
            <div class="whyus">
                <?php foreach ($content['field_blok_element']['#items'] as $key=>$val):?>
                <dl>
                    <dt><?php print $key+1;?>.</dt>
                    <dd><?php print $content['field_blok_element'][$key]['#markup']?></dd>
                </dl>
                             
                <?php endforeach;?>
            </div>
        </div>
        <?php }?>
    </div>
<?php if($body==''):?>
</div>
<?php endif; ?>