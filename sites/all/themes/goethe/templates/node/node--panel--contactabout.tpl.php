
<div class="kursort">
    <div class="cont">
        <h3><?php print $node->title; ?></h3>
        <ul>
            <li>
                <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-tel.gif" alt=""></span>
                <span class="text"><?php print render($content['field_telephone']); ?></span>
            </li>
            <li>
            <?php if(isset($content['field_fax'])): ?>
                <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-fax.gif" alt=""></span>
                <span class="text"><?php print render($content['field_fax']); ?></span>
            <?php endif; ?>
            </li>
            <li>
            <?php if(isset($content['field_e_mail'])): ?>
                <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-email.gif" alt=""></span>
                <?php $email = render($content['field_e_mail']); ?>
                <span class="text"><a href="mailto:<?php print $email; ?>"><?php print $email; ?></a></span>
            <?php endif; ?>
            </li>
            <li>
                <?php if(isset($content['field_qq'])): ?>
                    <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-qq.gif" alt=""></span>
                    <span class="text"><?php print render($content['field_qq']); ?></span>
                <?php endif; ?>
            </li>
            <li>
                <?php if(isset($content['field_msn'])): ?>
                <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-msn.gif" alt=""></span>
                <span class="text"><?php print render($content['field_msn']); ?></span>
                <?php endif; ?>
            </li>
            <li>
                <span class="ico"><img src="/sites/all/themes/goethe/images/ico/ico-address-contact.png" alt=""></span>
                <span class="text"><?php print render($content['field_address']); ?></span>
            </li>
        </ul>
        <?php if(isset($content['field_opening_hours'])): ?><p><strong><?php print t('Opening Hours'); ?>: </strong><?php print render($content['field_opening_hours']); ?></p><?php endif; ?>
        <?php if(isset($content['field_transport'])): ?><p><strong><?php print t('Transportation'); ?>: </strong><?php print render($content['field_transport']); ?></p><?php endif; ?>
    </div>
    <div class="followus">
        <ul>
            <?php if(isset($content['field_weixin'])): ?>
            <li class="weixin"><a></a><div class="weixin-content" style="display: none;"><?php print render($content['field_weixin']); ?><br/><?php print render($content['field_qr_code']); ?></div></li>
            <?php endif; ?>
            <?php if(isset($content['field_weibo'])): ?>
            <li class="weibo"><a target="_blank" href="<?php print render($content['field_weibo']); ?>"></a></li>
            <?php endif; ?>
            <?php if(isset($content['field_douban'])): ?>
            <li class="douban"><a target="_blank" href="<?php print render($content['field_douban']); ?>"></a></li>
            <?php endif; ?>
            <?php if(isset($content['field_boke'])): ?>
            <li class="boke"><a target="_blank" href="<?php print render($content['field_boke']); ?>"></a></li>
            <?php endif; ?>
            <?php if(isset($content['field_renren'])): ?>
            <li class="renren"><a target="_blank" href="<?php print render($content['field_renren']); ?>"></a></li>
            <?php endif; ?>
            <?php if(isset($content['field_blog'])): ?>
            <li class="blog"><a target="_blank" href="<?php print render($content['field_blog']); ?>"></a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>

