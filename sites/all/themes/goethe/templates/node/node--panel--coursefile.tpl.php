<?php
// get node from other domain if available
list($node, $content) = domain_translate($node, $content);
?>
<div class="rightlist">
    <div class="title"><h2><?php print $node->title; ?></h2></div>
    <div class="cont">
        <div class="terms">
            <div class="btn"><a style="margin-top:0;" target="_blank" href="<?php print render($content['field_file']); ?>" class="btn_down"><img src="/sites/all/themes/goethe/images/ico/ico-down2.png" alt=""> <?php print t('Download PDF'); ?></a></div>
        </div>
    </div>
</div>