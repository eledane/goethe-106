<div class="info">
    <?php print render($content['field_intro']); ?>
</div>
<div class="aboutus">
                	
    <div class="btn">
        <?php print l(t('Meet our team'), 'team'); ?>
        <?php print l(t('Check our Facilities'), 'facilities'); ?>
        <?php print l(t('We are hiring'), 'jobs'); ?>
    </div>
    <div class="title">
        <h2><?php print $node->title;?></h2>
    </div>
    <div class="text">
        <p><?php print render($content['body']);?></p>
                        
    </div>
    <div class="list">
        <?php $area=count(@$content['field_multiple_areas']['#items']);?>
        <?php if($area): ?>
        <?php foreach ($content['field_multiple_areas']['#items'] as $key=>$val):?>
            <dl <?php if($area==$key+1):?>class="last"<?php endif;?>>
                <dt><?php print $key+1;?></dt>
                <dd style="font-size:22px;">
                    <?php print render($content['field_multiple_areas'][$key])?>
                </dd>
            </dl>
            <?php endforeach;?>
        <?php endif; ?>
    </div>
</div>