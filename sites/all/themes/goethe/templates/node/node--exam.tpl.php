<?php
        $level = node_load(render($content['field_exam_level']));
        $level_view = node_view($level);

    $in_house = render($content['field_in_house_price']);
    $external = render($content['field_external_price']);
        ?>
<div class="title"><h2><?php print t('Dear Madam or Sir, we have received your information and will contact you directly within the next two working days, in order to facilitate further steps in the registration process. In case you do not hear from us within two days, please get in touch with us through phone or email.'); ?></h2></div>
<div class="prufungen_list">
    <ul>
        <li>
            <div class="block">
                <h3><?php print render($level_view['field_subtitle']); ?></h3>
                <p><?php print $level->title; ?></p>
            </div>
            <div class="block">
                <h4><?php print t('Internal'); ?></h4>
                <?php if($in_house): ?><div class="price"><span><?php print $in_house; ?></span><?php print t('RMB'); ?></div>
                <?php else: ?><div class="price"><span style="font-size:15px;"><?php print t('Included in the course fee'); ?></span></div><?php endif; ?>
                 </div>
            <div class="block" style="margin-right: 0px;">
                <h4><?php print t('External'); ?></h4>
                <?php if($external): ?><div class="price"><span><?php print $external; ?></span><?php print t('RMB'); ?></div>
                <?php else: ?><div class="price"><span style="font-size:15px;"><?php print t('Included in the course fee'); ?></span></div><?php endif; ?>
            </div>
            <div class="clearfix"></div>
            <p class="f14">
                <?php if(isset($level_view['field_exam_location'])): ?>
                    <strong class="green"><?php print t('Place'); ?>:</strong>  <?php print render($level_view['field_exam_location']); ?> (<strong><?php print t('Available Now'); ?></strong>)<br>
                <?php endif; ?>

                <strong class="green"><?php print t('Date'); ?>:</strong>   <?php print render($content['field_exam_date']); ?><br>

                <?php if(isset($level_view['field_place_of_registration'])): ?>
                    <strong class="green"><?php print t('Place of Registration'); ?>:</strong>   <?php print render($level_view['field_place_of_registration']); ?><br>
                <?php endif; ?>

                <?php if(isset($level_view['field_telephone_address'])): ?>
                    <strong class="green"><?php print t('Tel'); ?>:</strong> <?php print render($level_view['field_telephone_address']); ?><br>
                <?php endif; ?>

                <?php if(isset($level_view['field_email_address'])): ?>
                    <strong class="green"><?php print t('E-mail'); ?>:</strong> <?php print render($level_view['field_email_address']); ?>
                <?php endif; ?>
            </p>
        </li>
    </ul>
</div>

