<div class="title">
    <h2><?php print $node->title; ?></h2>
    <div class="back" style="width:60px;text-align: right;">< <?php print l(t('Back'), 'news'); ?></div>
</div>
<div class="news_cont<?php if(!isset($content['field_article_image'])) { print ' no-pic'; } ?>">
    <div class="date"><?php print t('Time');?>: <?php print date('Y - m -d',strtotime(render($content['field_publish_date'])));?> </div>
    <div class="photo"><?php print render($content['field_article_image']); ?></div>
    <?php print render($content['body']);?>
</div>