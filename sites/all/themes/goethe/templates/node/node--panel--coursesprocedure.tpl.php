<?php list($node, $content) = domain_translate($node, $content);
$body=render($content['body']);?>

<div class="rightlist">
    <div class="title"><h2><?php print $node->title; ?></h2></div>
    
    <div class="cont">
        <div class="whyus">
            <?php foreach ($content['field_blok_element']['#items'] as $key=>$val):?>
            <dl>
                <dt><?php print $key+1;?>.</dt>
                <dd><h5><?php print $content['field_blok_element'][$key]['#markup']?></h5></dd>
            </dl>
                             
            <?php endforeach;?>
        </div>
    </div>

</div>
