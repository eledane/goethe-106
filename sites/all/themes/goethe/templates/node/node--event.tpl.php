<?php if($_GET['q']!='events'): ?>
<div class="title">
    <h2><?php print $node->title; ?></h2>
    <div class="back" style="width:60px;text-align: right;"><a href="<?php print url('events'); ?>">< <?php print t('Back');?> </a></div>
</div>
<div class="kultur_cont">
    <p>
        <?php if($content['field_event_date']): ?><?php print t('Time');?>: <?php print render($content['field_event_date']);?><br/><?php endif; ?>
        <?php if($content['field_address_at']): ?><?php print t("Address");?>: <?php print render($content['field_address_at']);?><br/><?php endif; ?>
        <?php if($content['field_contact']): ?><?php print t("Contact");?>: <?php print render($content['field_contact']);?><?php endif; ?>
    </p>
    <?php print render($content['body']);?>
    <?php if(count(@$content['field_file']['#items'])): ?>
        <div class="btn">
            <a href="<?php print render($content['field_file']); ?>" class="btn_down"><img src="/sites/all/themes/goethe/images/ico/ico-down2.png" alt=""> <?php print t('Download'); ?></a>
        </div>
    </div>
    <?php endif; ?>
<?php else: ?>
<li>
    <dl>
        <dt><?php print render($content['field_event_image']); ?></dt>
        <dd>
            <h4><a href="<?php print url('node/' . $node->nid); ?>" class="green"><?php print $node->title; ?></a></h4>
            <div class="time"><?php print render($content['field_event_date']); ?></div>
            <div class="address"><?php print render($content['field_address_at']); ?></div>
            <div class="name"><?php print render($content['field_contact']); ?></div>
        </dd>
    </dl>
    <p><?php $body = render($content['body']);
    $alter = array('ellipsis' => true, 'max_length' => 500);
    print views_trim_text($alter, strip_tags($body));
    ?></p><br/>
    <div class="btn">
        <a href="<?php print url('node/' . $node->nid) ?>" class="more"><?php print t('More'); ?><img src="/sites/all/themes/goethe/images/ico/ico-more.png" alt=""></a>
    </div>
</li>
<?php endif; ?>
