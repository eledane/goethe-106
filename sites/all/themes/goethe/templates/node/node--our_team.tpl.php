<?php
if(strip_tags(render($content['field_team_type'])) == 'worker') {
    $type = 'worker';
} else {
    $type = 'teacher';
} ?>
<div class="title">
    <?php if($type == 'teacher'): ?>
        <h2><?php print t('Our Teachers'); ?></h2>
        <div class="back"><a href="<?php print url('team'); ?>">< <?php print t('Back'); ?></a></div>
    <?php else: ?>
        <h2><?php print t('Our Management Staff'); ?></h2>
        <div class="back"><a href="<?php print url('about'); ?>">< <?php print t('Back'); ?></a></div>
    <?php endif; ?>
</div>
<div class="ourTeachers">
    <div class="photo"><?php print render($content['field_photo'])?></div>
    <div class="text" <?php if(!$content['field_photo']) { print 'style="margin-left:0;"'; } ?>>
        <h3><?php print $node->title?></h3>
        <?php print render($content['body'])?>
    </div>
</div>