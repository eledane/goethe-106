<?php if($rows): ?>
<div class="rightlist">
    <div class="title"><h2><?php print t('Featured Events'); ?></h2></div>
    <div class="cont">
        <div class="veranstaltungen">
            <ul>
               <?php print $rows; ?>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>