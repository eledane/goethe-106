<dl>
    <dt><?php print $fields['field_cover_picture']->content; ?></dt>
    <dd>
        <h4><?php print $fields['title']->content; ?></h4>
        <div class="date"><?php print $fields['field_publish_date']->content; ?></div>
        <div class="text">
            <?php print $fields['body']->content; ?>
        </div>
        <div class="btn">
            <a href="<?php print $fields['field_download_pdf']->content; ?>" class="btn_down"><img src="/sites/all/themes/goethe/images/ico/ico-down2.png" alt=""> <?php print t('Download PDF'); ?></a>
        </div>
    </dd>
</dl>