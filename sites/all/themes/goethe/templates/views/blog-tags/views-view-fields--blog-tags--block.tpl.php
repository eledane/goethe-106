<?php
$count = tag_count($fields['tid']->content);
if($count > 5) {
    $class = 'b';
}
if($count > 10) {
    $class = 'f14 b';
}
?>
<li class="<?php print $class; ?>"><?php print l($fields['name']->content, 'blog/' . $fields['tid']->content); ?></li>