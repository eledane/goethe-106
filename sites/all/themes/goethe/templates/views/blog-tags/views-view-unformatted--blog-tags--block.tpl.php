<div class="rightlist">
    <div class="title"><h2><?php print t('Tags');?></h2></div>
    <div class="cont">
        <ul	class="tags">
            <li><?php print l(t('All'), 'blog'); ?></li>
            <?php foreach ($rows as $id => $row): ?>
            <?php print $row; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>