<dl>
    <dd class="nopicture">
        <h3><?php print $fields['title']->content; ?></h3>
        <p><?php print $fields['body']->content?></p>
        <div class="btn">
            <a href="<?php print url("node/".$fields['nid']->content);?>" class="more"><?php print t('More');?> <img src="/sites/all/themes/goethe/images/ico/ico-more.png" alt=""></a>
        </div>
    </dd>
</dl>