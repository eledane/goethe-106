<?php if(!$rows): ?>
<div class="title">
    <h2><?php print t('Search Result');?></h2>
</div>
<div class="news_list">
    <?php print t('No results found'); ?>
</div>
<?php else: ?>
    <?php print $rows; ?>
<?php endif; ?>
<?php print $pager; ?>