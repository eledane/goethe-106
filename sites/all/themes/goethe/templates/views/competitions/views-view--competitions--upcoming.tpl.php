<div class="list">
    <div class="title">
        <h3><a href="<?php print url('competitions'); ?>" class="active"><?php print t('Upcoming Competitition'); ?></a></h3>
        <h3 class="last"><a href="<?php print url('competitions', array('query' => array('v' => 'pst'))); ?>"><?php print t('Past Competition'); ?></a></h3>
    </div>
    <?php print $rows; ?>
</div>