<dl>
    <dt><?php print $fields['field_competition_pictures']->content; ?></a></dt>
    <dd>
        <h4><?php print $fields['title']->content; ?></h4>
        <div class="timeAddress">
            <div class="time"><img src="/sites/all/themes/goethe/images/ico/ico-time.png" alt=""> <?php print $fields['field_competition_date']->content; ?></div>
            <div class="address"><img src="/sites/all/themes/goethe/images/ico/ico-address.png" alt=""> <?php print $fields['field_address_at']->content; ?></div>
        </div>
        <div class="text">
            <?php print $fields['body']->content; ?>
        </div>
        <div class="btn">
            <a href="<?php print $fields['field_flyer']->content; ?>" class="btn_down"><img src="/sites/all/themes/goethe/images/ico/ico-down2.png" alt=""> <?php print t('Download Flyer'); ?></a>
        </div>
    </dd>
</dl>