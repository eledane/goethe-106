<div class="<?php print $classes; ?>">
    <div class="right">
        <div class="rightlist">
            <div class="title"><h2><?php print t('Press'); ?></h2>
                <div  style="display: none" id='realpage'><?php print $pager; ?></div>
                <div class="btn" id='presspager'>
                    <div class="prev"><a href="#" ><img src="/sites/all/themes/goethe/images/base/btn_prev_press2.png" alt="" /></a></div>
                    <div class="next"><a href="#"><img src="/sites/all/themes/goethe/images/base/btn_next_press2.png" alt="" /></a></div>
                </div>
            </div>
            <?php print $rows; ?>
        </div>
    </div>
</div>
