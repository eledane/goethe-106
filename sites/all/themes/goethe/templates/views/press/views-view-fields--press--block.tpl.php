<?php $url = $fields['field_files']->content ? $fields['field_files']->content : $fields['field_files_link']->content;
if(!$url) {
    $url = url('media');
}
?>
<h4>
    <a href="<?php print $url; ?>" class="green"><?php print $fields['title']->content; ?></a>
</h4>
<div class="b"><?php print $fields['field_media_outlet']->content; ?></div>
<div class="date"> <?php print $fields['field_press_date']->content; ?></div>
<div class="web"><a href="http://<?php print $fields['field_links']->content?>" class="green" target="_blank"><u><?php print $fields['field_links']->content?></u></a></div>
