<div class="cont">
    <div class="press">
        <ul>
            <?php foreach ($rows as $id => $row): ?>
            <li<?php if($id == count($rows) -1) { print ' class="last"'; } ?>>
                <?php print $row; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>