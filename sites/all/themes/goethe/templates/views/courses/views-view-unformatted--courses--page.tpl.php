<?php $parts = explode('/', $_GET['q']);
$level = taxonomy_term_load($parts[1]);
$terms = i18n_taxonomy_localize_terms(array($level));
$level = reset($terms);
$title = $level->name;
?>
<div class="title">
    <h2><?php print $title; ?></h2>
    <div class="description"><?php print $level->description; ?></div>
</div>
<?php print implode('', $rows); ?>
