<?php if(!$fields['field_level']->content && !$fields['field_duration']->content) { $short = true; } else { $short = false; } ?>

<?php if(!$short): ?>
<div class="rightlist">
    <div class="title"><h2><?php print $fields['title']->content; ?></h2></div>
    <div class="cont">
                        	
        <div class="IntensiveWeekendCourse">
            <div class="list">
                <dl>
                    <dt><?php print t('Duration');?>: </dt>
                    <dd><?php print $fields['field_duration']->content?></dd>
                </dl>
                <dl>
                    <dt><?php print t('Progress');?>:</dt>
                    <dd><?php print $fields['field_progress']->content?></dd>
                </dl>
                <dl>
                    <dt><?php print t('Level:'); ?></dt>
                    <dd>
                        <?php $i = 1; $lines = explode('||', $fields['field_level']->content); foreach($lines as $line): ?>
                        <?php if(strlen($line) > 8) { break; } ?>
                        <?php $short_lines[] = $line; $i++; ?>
                        <?php endforeach;?>
                        <?php print implode(', ', $short_lines); ?>
                    </dd>
                </dl>
                
                <dl>
                    <dt><?php print t('Description:'); ?></dt><dd>
                    <?php for($j = $i; $j < count($lines); $j++): ?>
                    <?php print $lines[$j];?><br/>
                    <?php endfor; ?>
                </dd>
                </dl>

                <dl>
                    <dt><?php print t('Group Size');?>:</dt>
                    <dd><?php print $fields['field_group_size']->content?></dd>
                </dl>
                <div class="price"><span><?php print $fields['field_price']->content?></span><?php print t('RMB');?></div>
                <dl style="clear:both; padding-top:10px;"><dd><?php print $fields['field_price_terms']->content?></dl></dd>
            </div>
        </div>
                            
    </div>
</div>
<?php endif; ?>
