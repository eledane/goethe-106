<div class="left appThankyou">
                                   	
    <div class="title"><h2><?php print $fields['title']->content; ?></h2></div>
    <div class="list">
        <div class="left">
            <dl>
                <dt><?php print t('Duration');?>: </dt>
                <dd><?php print $fields['field_duration']->content?></dd>
            </dl>
            <dl>
                <dt><?php print t('Progress');?>:</dt>
                <dd><?php print $fields['field_progress']->content?></dd>
            </dl>
            <?php $level=$fields['field_level']->content;
            $level_element=explode('||', $level) ;
            $num=count($level_element);
            $last_item=$level_element[$num-1];
            ?>
            <dl>
                <dt>Level:</dt>
                <dd>
                    <table width="200" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <?php for($i=0;$i<$num-1;$i++):?>
                            <?php if(($i%3)==0):?>
                                  
                            <tr>
                                <?php endif;?>
                            <td><?php print $level_element[$i]?></td>
                            <?php if((($i+1)%3)==0):?>
                                     
                            </tr>
                                <?php endif;?>
                            <?php endfor;?>
                        </tbody></table>
                </dd>
            </dl>
            <dl>
                <dd><?php print $last_item;?></dd>
            </dl>
            <dl>
                <dt><?php print t('Group Size');?>:</dt>
                <dd><?php print $fields['field_group_size']->content?></dd>
            </dl>
                             
        </div>
        <div class="right">
            <div class="price"><span><?php print $fields['field_price']->content?></span><?php print t('RMB');?></div>
            <p><?php print $fields['field_price_terms']->content?></p>
        </div>
    </div>
