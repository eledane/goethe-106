<?php if(!$fields['field_level']->content && !$fields['field_duration']->content) { $short = true; } else { $short = false; } ?>

<div class="list">
        <?php if(!$short): ?>
        <div class="left">
        <h3><a href="<?php print url('node/'.$fields['nid']->content)?>" class="green"><?php print $fields['title']->content; ?></a></h3>
        <dl>
            <dt><?php print t('Duration');?>: </dt>
            <dd><?php print $fields['field_duration']->content?></dd>
        </dl>

        <dl>
            <dt><?php print t('Progress');?>:</dt>
            <dd><?php print $fields['field_progress']->content?></dd>
        </dl>

        <dl>
            <dt><?php print t('Level:'); ?></dt>
            <dd>
                <?php $i = 1; $lines = explode('||', $fields['field_level']->content); foreach($lines as $line): ?>
                <?php if($line[0] == '*') { break; } ?>
                <?php $short_lines[] = $line; $i++; ?>
                <?php endforeach;?>
                <?php print implode(', ', $short_lines); ?>
            </dd>
        </dl>
                
        <dl>
            <dt><?php print t('Description:'); ?></dt><dd>
            <?php for($j = $i-1; $j < count($lines); $j++): ?>
                <?php print $lines[$j];?><br/>
            <?php endfor; ?>
            </dd>
        </dl>

        <dl>
            <dt><?php print t('Group Size');?>:</dt>
            <dd><?php print $fields['field_group_size']->content?></dd>
        </dl>
        
        <div class="btn">
            <a href="<?php print url('node/'.$fields['nid']->content)?>" class="more"><?php print t("More");?> <img src="/sites/all/themes/goethe/images/ico/ico-more.png" alt=""></a>
        </div>
        <?php else: ?>
        <div class="left shortstyle">
        <h3><span class="green"><?php print $fields['title']->content; ?></span></h3>
        <dl>
            <dd style="width:100%"><?php print $fields['body']->content?></dd>
        </dl>
        <?php endif; ?>
    </div>
    <div class="right">
        <?php if(!$short): ?>
        <div class="price"><span><?php print $fields['field_price']->content?></span> <?php print t('RMB');?></div>
        <p><?php print $fields['field_price_terms']->content?></p>

         <?php if($fields['field_course_status']->content == 'book online'):?>
            <div class="btn"><a href="<?php print url('book-course/'.$fields['nid']->content);?>" class="btn_anmeldung"><?php print t('Register'); ?></a></div>
            <?php else: ?>
            <div class="btn"><a href="javascript:void(0)" class="btn_anmeldung"><?php print t('Fully Booked'); ?></a></div>
        <?php endif; ?>
        <?php else: ?>
            <div class="btn"><a style="margin-top: 0;" href="<?php print url('book-course/'.$fields['nid']->content);?>" class="btn_anmeldung"><?php print t('Signup'); ?></a></div>
        <?php endif; ?>



    </div>
</div>
