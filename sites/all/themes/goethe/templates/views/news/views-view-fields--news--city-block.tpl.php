<?php $more_link = ' ' . l(t('Read more >'), 'node/' . $fields['nid']->content, array('attributes' => array('class' => array('green')))); ?>
<dd>
    <h3 class="green"><?php print $fields['title']->content; ?></h3>
    <div class="date"><?php print date('Y-m-d',strtotime(strip_tags($fields['field_publish_date']->content))); ?></div>
    <p><?php print $fields['body']->content; ?><?php print $more_link; ?></p>
</dd>