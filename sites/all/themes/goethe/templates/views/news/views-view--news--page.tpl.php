<div style="display:none"><?php echo $exposed?></div>
<div class="newsbox">
    <div class="title">
        <h2 style="width:auto;"><?php print t("News");?></h2>
        <?php  $q= isset($_GET['date']['value']['year']) ? $_GET['date']['value']['year'] : 'all';?>
        <ul class="year">
            <li><a href="<?php print url('news'); ?>" class="<?php print ($q=='all' ? 'active' : ''); ?>"><?php print t('All'); ?></a></li>
            <li><a href="<?php print url('news'); ?>?date%5Bvalue%5D%5Byear%5D=2013" class="<?php print ($q==2013 or $q=='')?'active':'';?>">2013</a></li>
            <li><a href="<?php print url('news'); ?>?date%5Bvalue%5D%5Byear%5D=2012"  class="<?php print $q==2012?'active':'';?>">2012</a></li>
           
        </ul>
    </div>

    <?php print $rows; ?>
    <?php print $pager; ?>
</div>
