<dl>
    <?php if($fields['field_article_image']->content): ?>
    <dt><a href="<?php print url('node/' . $fields['nid']->content);?>"><?php print $fields['field_article_image']->content; ?></a></dt>
    <dd>
    <?php else: ?>
    <dd class="nopicture">
    <?php endif; ?>
        <h3><?php print $fields['title']->content; ?></span></h3>
        <div class="date"><?php print date('Y-m-d',strtotime(strip_tags($fields['field_publish_date']->content))); ?></div>
        <p><?php 
        //print $fields['body']->content;
        $body = $fields['body']->content;         
        $find = array("&amp;nbsp", "&nbsp");
        echo str_replace($find, "", $body);
        ?></p>
       
        <div class="btn">
            <a href="<?php print url('node/' . $fields['nid']->content);?>" class="more"><?php print t('More'); ?> <img src="/sites/all/themes/goethe/images/ico/ico-more.png" alt=""></a>
        </div>
    </dd>
</dl>
