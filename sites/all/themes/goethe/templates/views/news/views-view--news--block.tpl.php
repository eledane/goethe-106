<div class="home_news" style="margin-bottom:40px;">
    <div class="cont">
        <div class="title">
            <h2><?php print t('News'); ?></h2>
            <div class="more"><a href="<?php print url('news'); ?>"><?php print t('More'); ?></a></div>
        </div>
        <?php print $rows; ?>
    </div>
</div>