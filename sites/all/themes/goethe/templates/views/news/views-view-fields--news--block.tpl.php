<?php $more_link = '<br/>' . l(t('Read more >'), 'node/' . $fields['nid']->content, array('attributes' => array('class' => array('green')))); ?>
<dl>
    <dt><?php print str_replace('<a', '<a class="green"', $fields['title']->content); ?></dt>
    <dd><?php print append_to_last_p($more_link, $fields['body']->content); ?></dd>
</dl>