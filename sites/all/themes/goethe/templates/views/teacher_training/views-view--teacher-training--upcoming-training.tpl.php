<div class="list">
    <div class="title">
        <h3><a href="<?php print url('training'); ?>" class="active"><?php print t('Upcoming Training'); ?></a></h3>
        <h3 class="last"><a href="<?php print url('training', array('query' => array('v' => 'pst'))); ?>"><?php print t('Past training'); ?></a></h3>
    </div>
    <?php print $rows; ?>
</div>