<dl>
    <dt><?php print $fields['field_picture']->content; ?></dt>
    <dd>
        <h4><?php print $fields['title']->content; ?></h4>
        <div class="timeAddress">
            <div class="time"><img src="/sites/all/themes/goethe/images/ico/ico-time.png" alt=""> <?php print $fields['field_traninging_time']->content; ?></div>
            <div class="address"><img src="/sites/all/themes/goethe/images/ico/ico-address.png" alt=""> <?php print $fields['field_address_at']->content; ?></div>
        </div>
        <div class="text">
            <?php print $fields['body']->content; ?>
        </div>
    </dd>
</dl>