<div class="kTitle t40">
    <h2><?php print t('From the Blog'); ?></h2>

    <div class="btn_blog"><a href="<?php print url('blog'); ?>"><?php print t('Blog'); ?> <img src="/sites/all/themes/goethe/images/ico/ico-arrow.png" alt=""></a></div>
</div>
<div class="blog_list">
    <?php print $rows; ?>
</div>