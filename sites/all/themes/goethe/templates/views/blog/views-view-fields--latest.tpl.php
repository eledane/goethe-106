<?php $more_link = ' ' . l(t('Read more >'), 'node/' . $fields['nid']->content, array('attributes' => array('class' => array('green')))); ?>
<dt><?php print $fields['field_related_picture']->content; ?></dt>
<dd>
    <h3><a href="<?php print url('node/' . $fields['nid']->content); ?>" class="green"><?php print $fields['title']->content; ?></a></h3>
    <p><?php print $fields['body']->content; ?><?php print $more_link; ?></p>

</dd>