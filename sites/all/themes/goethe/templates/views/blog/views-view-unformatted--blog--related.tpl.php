<?php $i=1;?> 
<div class="title">
    <h2><?php print t('Related Articles');?></h2>
</div>
<div class="kultur_list">
    <?php foreach ($rows as $id => $row): ?>

    <?php   
    print '<dl' . ($i++ == count($rows) ? ' class="last"' : '') . '>';
    print $row;
    print '</dl>';?>

    <?php endforeach; ?>

</div>