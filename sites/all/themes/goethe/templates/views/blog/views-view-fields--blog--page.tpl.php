<dl>
    <dt><a href="<?php print url('node/' . $fields['nid']->content);?>"><?php print $fields['field_related_picture']->content; ?></a></dt>
    <dd>
        <h3><?php print str_replace('<a', '<a class="green"', $fields['title']->content); ?></h3>
        <p><?php print $fields['body']->content; ?></p>
       
        <div class="btn">
            <a href="<?php print url('node/' . $fields['nid']->content);?>" class="more"><?php print t("More");?> <img src="/sites/all/themes/goethe/images/ico/ico-more.png" alt=""></a>
        </div>
    </dd>
</dl>