<?php
$title = t('Recent articles');
$parts = explode('/', $_GET['q']);
if(isset($parts[1])) {
    $term = taxonomy_term_load($parts[1]);
    $term = i18n_taxonomy_localize_terms($term);
    $title = $term->name;
}
?>
<div class="left">
    <div class="kultur">

        <div class="title">
            <h2><?php print $title;?></h2>
        </div>
        <div class="kultur_list">
            <?php print $rows; ?>
            <?php print $pager;?>
        </div>
    </div>
</div>
  
