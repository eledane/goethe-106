<?php $class = strtolower($fields['title']->raw[0]); ?>
<dl>
    <dt><span class="<?php print $class; ?>"><?php print $fields['title']->content; ?></span></dt>
    <dd><?php print $fields['body']->content; ?></dd>
</dl>