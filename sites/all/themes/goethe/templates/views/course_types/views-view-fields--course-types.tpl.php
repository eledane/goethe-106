<?php
$term = taxonomy_term_load($fields['tid']->content);
$has_nodes = term_has_nodes($term->tid, 'course');
$name = i18n_taxonomy_term_name($term);
if($has_nodes):
?>
<li>
    <a href="<?php print url('courses/' . $fields['tid']->content); ?>">
        <img src="/sites/all/themes/goethe/images/ico/ico-start.png" alt="">
        <?php print $name; ?>
    </a>
</li>
<?php endif; ?>