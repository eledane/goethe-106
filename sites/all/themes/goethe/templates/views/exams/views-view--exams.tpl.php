<div class="<?php print $classes; ?>">
    <div class="kTitle t20"><h2><?php print t('Exam agenda'); ?></h2></div>
    <div class="exam_slide" id="exam_slide">
        <div class="view-content">
            <div class="list">

                <?php
                if($rows) {
                    print $rows;
                } else {
                    print display_agenda(agenda_start_date($view), array());
                }
                ?>
                <?php if ($exposed): ?>
                    <div class="view-filters" style="width: 80%;position: absolute;top: 0;overflow: hidden;">
                        <?php print $exposed; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div style="display:none"><?php print $pager; ?></div>
        <a href="#" class="prev">Prev</a>
        <a href="#" class="next">Next</a>
    </div>
</div>