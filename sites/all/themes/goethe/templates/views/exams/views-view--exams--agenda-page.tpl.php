<div class="<?php print $classes; ?>">
    <div class="title"><h2><?php print t('Exam Agenda'); ?></h2></div>
    <div class="exam_slide" id="exam_slide">

        <div class="view-content">
            <div class="list">
                <?php if ($exposed): ?>

                    <div class="view-filters">

                        <?php print $exposed; ?>

                    </div>

                <?php endif; ?>
                <?php
                if($rows) {
                    print $rows;
                } else {
                    print display_agenda(agenda_start_date($view), array(), true);
                }
                ?>
            </div>

        </div>

        <div style="display:none"><?php print $pager; ?></div>
        <a href="#" class="prev">Prev</a>
        <a href="#" class="next">Next</a>
    </div>
</div>



