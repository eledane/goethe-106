<?php
foreach($rows as $row) {
    $exam = explode('|', $row);
    // place row in correct place in week array
    $week[intval(date('d', $exam[0]))][] = $exam;
    // generate week table
}

print display_agenda(agenda_start_date($view), $week);
