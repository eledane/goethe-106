<div class="rightlist">
    <div class="title"><h2><?php print t('Testimonials'); ?></h2></div>
    <div class="cont">
        <div class="testimonials" id="testimonials_slide">
            <div class="slides_container">
                <?php print $rows; ?>
            </div>
        </div>
                        
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#testimonials_slide').slides({
            preload: true,
            generateNextPrev: true,
            generatePagination:false,
            play: 0,
            pause: 2500,
            autoHeight: true
        });
        jQuery('#testimonials_slide a.prev').click();
    });
</script>