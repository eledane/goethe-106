<?php
if(!function_exists( 'first_time')) {
    function first_time(){
        static $first_time = true;
        if($first_time) {
            $first_time = false;
            return true;
        }
        return false;
    }
}
?>
<dl>
    <dt><span><?php print $fields['title']->content;?></span></dt>
    <dd>
        <?php print $fields['body']->content;?>
    </dd>
</dl>
<?php if(!first_time()):?>
<p>
    <?php print $fields['field_baidu_map']->content?>
</p>
<?php endif;?>
<script type="text/javascript">var baidu_map_limit = 0;</script>