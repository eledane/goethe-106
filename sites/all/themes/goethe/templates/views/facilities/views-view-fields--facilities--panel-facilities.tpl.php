<div class="list">
    <div class="bg"><?php print $fields['field_picture']->content; ?></div>
    <div class="contents">
        <div class="cont">
            <h2><?php print $fields['title']->content; ?></h2>
            <p><?php print $fields['body']->content; ?></p>
        </div>
    </div>
</div>
