<div id="lemumgebung">
    <div class="slides_container">
        <?php print $rows; ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#lemumgebung').slides({
            preload: true,
            generateNextPrev: true,
            generatePagination:false,
            play: 0,
            pause: 2500
        });
    });
</script>