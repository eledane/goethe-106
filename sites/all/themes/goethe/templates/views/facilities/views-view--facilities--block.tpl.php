<div class="kTitle"><h2><?php print t('Facilities'); ?></h2></div><br/><br/>
<div class="kursen_slide" id="kursen_slide">
    <div class="slides_container">
        <?php print $rows; ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#kursen_slide').slides({
            preload: true,
            generateNextPrev: false,
            play: 0,
            pause: 2500,
            start: 0
        });
        jQuery('#kursen_slide .pagination a:eq(2)').click();
        jQuery('#kursen_slide .pagination q:eq(0)').click();
    });
</script>