<?php
$i = 1;
foreach($rows as $row) {
    if(trim($row)) {
        print '<li' . ($i++ == count($rows) ? ' class="last"' : '') . '>';
        print $row;
        print '</li>';
    }
}
