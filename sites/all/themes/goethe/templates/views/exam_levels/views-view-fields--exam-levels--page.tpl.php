<?php
    $exam = load_exam($fields['nid']->content);
if($exam):
    $exam_view = node_view($exam);
    $es = render($exam_view['field_exam_status']);
    $in_house = render($exam_view['field_in_house_price']);
    $external = render($exam_view['field_external_price']);
?>
<div class="block">
    <h3><?php print $fields['field_subtitle']->content; ?></h3>
    <p><?php print $fields['title']->content; ?></p>
</div>
<div class="block">
    <h4><?php print t('Internal'); ?></h4>
    <?php if($in_house): ?><div class="price"><span><?php print $in_house; ?></span><?php print t('RMB'); ?></div>
    <?php else: ?><div class="price"><span style="font-size:15px;"><?php print t('Included in the course fee'); ?></span></div><?php endif; ?>
</div>
<div class="block" style="margin-right: 0px;">
    <h4><?php print t('External'); ?></h4>
    <?php if($external): ?><div class="price"><span><?php print $external; ?></span><?php print t('RMB'); ?></div>
    <?php else: ?><div class="price"><span style="font-size:15px;"><?php print t('Included in the course fee'); ?></span></div><?php endif; ?>
</div>
<div class="clearfix"></div>
<p class="f14">
    <?php if(isset($exam_view['field_exam_location'])): ?>
    <strong class="green"><?php print t('Place'); ?>:</strong>  <?php print render($exam_view['field_exam_location']); ?> <br>
    <?php endif; ?>

    <?php /* <strong class="green"><?php print t('Date'); ?>:</strong>   <?php print render($exam_view['field_exam_date']); ?><br> */ ?>

    <?php if(isset($exam_view['field_place_of_registration'])): ?>
    <strong class="green"><?php print t('Place of Registration'); ?>:</strong>   <?php print render($exam_view['field_place_of_registration']); ?><br>
    <?php endif; ?>

    <?php if(isset($exam_view['field_telephone_address'])): ?>
    <strong class="green"><?php print t('Tel'); ?>:</strong> <?php print render($exam_view['field_telephone_address']); ?><br>
    <?php endif; ?>

    <?php if(isset($exam_view['field_email_address'])): ?>
    <strong class="green"><?php print t('E-mail'); ?>:</strong> <?php print render($exam_view['field_email_address']); ?>
    <?php endif; ?>

</p>
<p><strong class="f14"><?php print t('Brief'); ?>:</strong></p>
<div class="goethe-markdown-update"><?php print $fields['body']->content; ?></div>
<?php if ($es == 'book online'):?>
<div class="btn"><a href="<?php print url('book-exam/' . $fields['nid']->content); ?>" class="btn_book"><?php print t('Book'); ?></a></div>
<?php else: ?>
<div class="btn"><div class="btn_okk"><?php print t('Fully Booked'); ?></div></div>
<?php endif; ?>
<?php endif; ?>
