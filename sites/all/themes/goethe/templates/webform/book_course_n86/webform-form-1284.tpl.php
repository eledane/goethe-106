<?php
$parts = explode('/', $_GET['q']);
$course_level = node_load($parts[1]);
$all_levels = courses_levels();

if($course_level->type != 'course') {
    print 'Error'; exit;
}
$form['actions']['submit']['#attributes']['class'][] = 'btn_ok';

$form['submitted']['course_id']['#value'] = $parts[1];
$form['submitted']['starting']['#value'] = date('r');

$current_domain = domain_get_domain();

$terms_files = explode("\r\n", $form['submitted']['i_have_already_read_the_terms_and_conditions']['#description']);
$terms_file = 'javascript:;';
foreach($terms_files as $line) {
    $line = explode('|', $line);
    if(count($line) == 1) {
        $terms_file = $line[0]; break;
    }
    if($line[0] == $current_domain['domain_id']) { $terms_file = $line[1]; break; }
}

?>

<div class="left">
    <div class="title">
        <h2>
            <?php print t('Application Form for');?>
        </h2>
    </div>

    <div class="application">
        <div class="aTitle">
            <div class="cont">
                <h3><?php print $course_level->title;?></h3>
                <div class="ico"><a href="#"><img src="/sites/all/themes/goethe/images/ico/ico-app.png" alt=""></a></div>
            </div>
            <div class="aList">
                <ul>
                    <?php foreach($all_levels as $level): ?>
                        <?php $domains = domain_get_node_domains($level->nid);
                        if(!isset($domains['domain_id'][$current_domain['domain_id']])) continue; ?>
                    <li><a href="<?php print url('book-course/' . $level->nid); ?>"><?php print $level->title; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="form">
            <ul>
                <li>
                    <?php if(count($course_level->field_the_course_of_time_['und'])): ?>
                        <div class="name">
                            <?php print t('Starting'); ?>
                        </div>
                    <div class="input selectbox" id="time_select">
                        <input type="text" value="<?php print t('Loading'); ?>...">
                        <ul>
							<?php foreach($course_level->field_the_course_of_time_['und'] as $slot): ?>
                                <li><a href="#"><?php print $slot['value']; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                </li>
				
                <li>
                    <div class="name"><?php print $form['submitted']['full_name']['#title']; ?> <span class="green">*</span></div>
                    <?php print textfield($form['submitted'], 'full_name'); ?>
                </li>
				
                <li>
                    <div class="name">
                        <?php print $form['submitted']['e_mail']['#title']; ?> <span class="green">*</span>
                    </div>
                    <?php print textfield($form['submitted'], 'e_mail'); ?>
                    <div class="text" style="padding-left:145px;"><?php print t('We will use the address to contact you in the future');?></div>
                </li>
                <li>
                    <div class="name"><?php print $form['submitted']['phone']['#title']; ?></div>
                    <?php print textfield($form['submitted'], 'phone'); ?>
                </li>
                <li>
                    <div class="name"><?php print $form['submitted']['gender']['#title']; ?> <span class="green">*</span></div>
                    <?php print select($form['submitted'], 'gender', 'width:103px; background-position:95px center;'); ?>
                </li>
                <li>
                    <div class="name"><?php print $form['submitted']['date_of_birth']['#title']; ?> <span class="green">*</span></div>
                    <?php print datefield($form['submitted'], 'date_of_birth'); ?>
                </li>
                <li>
                    <div class="name"><?php print $form['submitted']['place_of_birth']['#title']; ?> <span class="green">*</span></div>
                    <?php print textfield($form['submitted'], 'place_of_birth'); ?>
                </li>
                <li>
                    <div class="name"><?php print $form['submitted']['id']['#title']; ?> <span class="green">*</span></div>
                    <?php print textfield($form['submitted'], 'id'); ?>
                </li>
                <li>
                    <div class="name"><?php print $form['submitted']['address']['#title']; ?></div>
                    <div class="input">
                        <?php print textarea($form['submitted'], 'address', 'width:395px;height:50px'); ?>
                    </div>
                </li>
				
				
                <li>
                    <div class="input">
                        <?php print textarea($form['submitted'], 'anything_you_would_like_to_add', 'height:100px'); ?>
                        <span class="inputText"><?php print t('Anything you would like to add?');?></span>
                    </div>
                </li>

                <li>
                    <?php
                        $text = $form['submitted']['i_have_already_read_the_terms_and_conditions']['#title'];
                        $text = str_replace('!lstart', '<a  class="file_a" target="_blank" href="/'.$terms_file.'">', $text);
                        $text = str_replace('!lend', '</a>', $text);
                        ?>
                        <ul class="checkboxSingle">
                            <li><?php print drupal_render($form['submitted']['i_have_already_read_the_terms_and_conditions']); ?><?php print $text; ?></li>
                            
                        </ul>
                </li>
                
            </ul>
            <div class="clearfix"></div>
            <div class="other">
                <h2><?php print t('Learning Experience'); ?></h2>
                <div class="left">
                    <?php print checkboxlist($form['submitted'], 'do_you_know_pinyin'); ?>
                </div>
                <div class="left">
                    <?php print checkboxlist($form['submitted'], 'do_you_know_english'); ?>
                </div>
                <div class="clearfix"></div>
                <div class="left" style="position:relative;">
                    <?php print checkboxlist($form['submitted'], 'have_you_learnt_german', true); ?>
                    <div style="position:absolute;left:65px;top:46px;">
                        <ul>
                            <li>
                                <?php print textfield($form['submitted'], 'if_so_when', 'width:130px;padding:8px 10px;'); ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="left">
                    <h4><?php print $form['submitted']['why_do_you_want_to_learn_german']['#title']; ?></h4>
                    <?php print select($form['submitted'], 'why_do_you_want_to_learn_german'); ?>
                </div>
                <div class="clearfix"></div>
                <div class="left" style="clear:both;">
                    <h4><?php print $form['submitted']['where_did_you_hear_about_us']['#title']; ?></h4>
                    <?php print select($form['submitted'], 'where_did_you_hear_about_us'); ?>
                </div>
                <div class="left">
                    <?php print checkboxlist($form['submitted'], 'do_you_want_to_take_the_start_deutsch_i_exam'); ?>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>
        <div class="btn"><?php print render($form['actions']['submit']); ?></div>


    </div>
</div>
<div style="display:none;">
    <?php print drupal_render_children($form); ?>
</div>
<script type="text/javascript">
    <?php $domain = domain_get_domain(); ?>
    var _form_title = 'Goethe Website | <?php print $domain['sitename']; ?> | NAME | <?php print $course_level->title; ?>';
</script>
<script type="text/javascript">
    function updateTitle() {
        var name = jQuery('input[name="submitted[full_name]"]').val();
        var new_title = _form_title.replace('NAME', name);
        jQuery('input[name="submitted[title]"]').val(new_title);
    }

    jQuery(document).ready(function() {
        $ = jQuery;

        $('#time_select li').click(function() {
            $('input[name="submitted[period]"]').val($(this).text());
        });

        $('#time_select li:eq(0)').click();

        // set default date
        $('.webform-calendar').click();
        $(".ui-datepicker").hide();
        $('body').click(); // take focus out of calendar input

        $('input[name="submitted[full_name]"]').live('change', updateTitle);
        updateTitle();

        $('.webform-client-form').submit(function() {
            var yes = $('#edit-submitted-i-have-already-read-the-terms-and-conditions input').attr('checked');

            if(!yes) {
                var div = $('#edit-submitted-i-have-already-read-the-terms-and-conditions');
                div.siblings('li').css('color', '#F44');
                var off = div.offset();
                div.siblings('li').animate({opacity:0},200,"linear",function(){
                    $(this).animate({opacity:1},200, "linear", function() {
                        $(this).animate({opacity:0},200, "linear", function() {
                            $(this).animate({opacity:1},200, "linear");
                        });
                    });
                });

                window.scrollTo(off.left, off.top - 150);
                return false;
            }
            return true;
        });
    });

</script>
