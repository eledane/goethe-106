<?php
$parts = explode('/', $_GET['q']);
$exam_level = node_load($parts[1]);
$all_levels = exam_levels();
if($exam_level->type != 'exam_level') {
    print 'Error'; exit;
}

$strintar = explode('/',$_GET['q']);
$nid = $strintar[2] ? intval($strintar[2]):0;
$form['actions']['submit']['#attributes']['class'][] = 'btn_ok';
//$form['submitted']['city']['#attributes']['class'][] = 'alex';
?>
<div class="title"><h2 style="font-size:29px;"><?php print t('I wish to register for'); ?></h2></div>
<div class="application">
    <div class="aTitle">
        <div class="cont">
            <h3><?php print $exam_level->title; ?></h3>
            <div class="ico"><a href="#"><img src="/sites/all/themes/goethe/images/ico/ico-app.png" alt=""></a></div>
        </div>
        <div class="aList">
            <ul>
                <?php foreach($all_levels as $level): ?>
                    <li><a href="<?php print url('book-exam/' . $level->nid); ?>"><?php print $level->title; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="form">
        <ul>
            <li>

		<div class="input" style="position: relative; z-index: 1;">
		    <select id="edit-submitted-city" name="" class="form-select">
		    </select>
		</div>


		<div class="input" style="position: relative; z-index: 1;">
		    <select id="edit-submitted-exam-date" name="" class="form-select">
		    </select>
		</div>


<!--
                <div class="input selectbox" id="location_select">
                    <input type="text" value="<?php print t('Loading'); ?>...">
                    <ul>
                    </ul>
                </div>

                <div class="input selectbox" style="visibility:hidden" id="time_select">
                    <input type="text" value="<?php print t('Loading'); ?>...">
                    <ul>
                    </ul>
                </div>

-->
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['name']['#title']; ?> <span class="green">*</span></div>
                <?php print textfield($form['submitted'], 'name'); ?>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['e_mail']['#title']; ?> <span class="green">*</span></div>
                <?php print textfield($form['submitted'], 'e_mail'); ?>
                <div class="text" style="padding-left:145px;"><?php print t('We will use the address to contact you in the future'); ?></div>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['phone']['#title']; ?> <span class="green">*</span></div>
                <?php print textfield($form['submitted'], 'phone'); ?>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['gender']['#title']; ?> <span class="green">*</span></div>
                <?php print select($form['submitted'], 'gender', 'width:103px; background-position:95px center;'); ?>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['date_of_birth']['#title']; ?> <span class="green">*</span></div>
                <?php print datefield($form['submitted'], 'date_of_birth'); ?>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['place_of_birth']['#title']; ?> <span class="green">*</span></div>
                <?php print textfield($form['submitted'], 'place_of_birth'); ?>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['nationality']['#title']; ?>  <span class="green">*</span></div>
                <?php print select($form['submitted'], 'nationality', 'width:230px; background-position:220px center;'); ?>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['id']['#title']; ?> <span class="green">*</span></div>
                <?php print textfield($form['submitted'], 'id'); ?>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['address']['#title']; ?></div>
                <div class="input">
                    <?php print textarea($form['submitted'], 'address', 'width:395px;height:50px'); ?>
                </div>
            </li>
            <li>
                <div class="name"><?php print $form['submitted']['postcode']['#title']; ?>  <span class="green">*</span></div>
                <div class="input"><?php print textfield($form['submitted'], 'postcode'); ?></div>
            </li>
            <li>
                <div class="input" style="position: relative; z-index: 1;">
                    <?php print textarea($form['submitted'], 'other_information', 'height:100px'); ?>
                </div>
            </li>
        </ul>
        <div class="clearfix"></div>
<!--        <div class="other">-->
<!--            <h2>--><?php //print t('Learning Experience'); ?><!--</h2>-->
<!--            <div class="left">-->
<!--                --><?php //print checkboxlist($form['submitted'], 'do_you_know_pinyin'); ?>
<!--            </div>-->
<!--            <div class="left">-->
<!--                --><?php //print checkboxlist($form['submitted'], 'do_you_know_english'); ?>
<!--            </div>-->
<!--            <div class="clearfix"></div>-->
<!--            <div class="left" style="position:relative;">-->
<!--                --><?php //print checkboxlist($form['submitted'], 'have_you_learnt_german', true); ?>
<!--                <div style="position:absolute;left:65px;top:46px;">-->
<!--                    <ul>-->
<!--                        <li>-->
<!--                            --><?php //print textfield($form['submitted'], 'if_so_when', 'width:130px;padding:8px 10px;'); ?>
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="left">-->
<!--                <h4>--><?php //print $form['submitted']['why_do_you_want_to_learn_german']['#title']; ?><!-- <span class="green">*</span></h4>-->
<!--                --><?php //print select($form['submitted'], 'why_do_you_want_to_learn_german'); ?>
<!--            </div>-->
<!--            <div class="clearfix"></div>-->
<!--            <div class="left">-->
<!--                 <h4>--><?php //print $form['submitted']['where_did_you_hear_about_us']['#title']; ?><!-- <span class="green">*</span></h4>-->
<!--                --><?php //print select($form['submitted'], 'where_did_you_hear_about_us'); ?>
<!--            </div>-->
<!--            <div class="left">-->
<!--            </div>-->
<!--        </div>-->

    </div>

    <div class="clearfix"></div>
    <div class="btn"><?php print render($form['actions']['submit']); ?></div>

</div>
<div style="display:none">
<?php print drupal_render_children($form); ?>
</div>
<script type="text/javascript">
    <?php $domain = domain_get_domain();?>
    var _form_title = 'Goethe Website | <?php print $domain['sitename']; ?> | NAME | <?php print $exam_level->title; ?>';
</script>

<script type="text/javascript">
    var exam_id = <?php echo $nid;?>;

    (function ($) {
        function updateTitle() {
            var name = jQuery('input[name="submitted[name]"]').val();
            var new_title = _form_title.replace('NAME', name);
            jQuery('input[name="submitted[title]"]').val(new_title);
        }

        $(document).ready(function() {

	   var flag = jQuery.isEmptyObject(booking_filter);

	   if ( flag == false  ){
            $.each(booking_filter, function(i, e) {
	       $('#edit-submitted-city option').remove()
               $('#edit-submitted-city').append('<option value="' + i +'">'+i+'</option>');
	    });
	   }else{
	   
               $('#edit-submitted-city').append('<option value=""><?php print t('Fully Booked'); ?></option>');
	   }


            $('#edit-submitted-city').live('click', function(e) {
                e.preventDefault();
                $('#edit-submitted-exam-date option').remove();
     
		if( flag == false ){
                $.each(booking_filter[$(this).val()], function(i, e) {
                    $('#edit-submitted-exam-date').append('<option id="exam_' + i +'" value="' + e +'">'+e+'</option>');
		});
		}else{
		
                    $('#edit-submitted-exam-date').append('<option value=""><?php print t('Fully Booked'); ?></option>');
		}

            });

	    $('#edit-submitted-city').trigger('click');

	    //for exam date
	    var exam_date = $('#edit-submitted-exam-date option:selected').val();
            $('input[name="submitted[exam_date]"]').val(exam_date);

            //for exam_id
	    var exam_id = $('#edit-submitted-exam-date option:selected').attr('id');
	    var exam_id_f = exam_id.replace('exam_', '');
            $('input[name="submitted[exam_id]"]').val(exam_id_f);

	    //for exam city
	    var exam_city = $('#edit-submitted-city option:selected').val();
            $('input[name="submitted[exam_city]"]').val(exam_city);


	    $('div.input').bind('change', "#edit-submit-exam-date", function(){
		var exam_date = $('#edit-submitted-exam-date option:selected').val();
		$('input[name="submitted[exam_date]"]').val(exam_date);

	     //for exam_id
	    var exam_id = $('#edit-submitted-exam-date option:selected').attr('id');
	    var exam_id_f = exam_id.replace('exam_', '');
            $('input[name="submitted[exam_id]"]').val(exam_id_f);

	    });


	    $('div.input').bind('click', "#edit-submit-city", function(){
		var exam_city = $('#edit-submitted-city option:selected').val();
		$('input[name="submitted[exam_city]"]').val(exam_city);
	    });


            $('input[name="submitted[name]"]').live('change', updateTitle);

            updateTitle();
          //  $('#location_select ul li a').first().click();

            if(exam_id > 0) {
                $('#time_select ul #exam_' + exam_id).click();
                updateTitle();
            }
        });
    })(jQuery);
</script>

