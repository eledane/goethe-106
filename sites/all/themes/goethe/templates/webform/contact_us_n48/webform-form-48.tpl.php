<?php
$form['actions']['submit']['#attributes']['class'][] = 'btn_submit';
?>
<div class="contactfrom">
    <div class="cont">
        <h3><?php print t('Contact Us'); ?></h3>
        <ul>
            <li>
                <?php print textfield($form['submitted'], 'full_name'); ?>
            </li>
            <li>
                <?php print textfield($form['submitted'], 'email'); ?>
            </li>
            <li>
                <?php print textfield($form['submitted'], 'phone'); ?>
            </li>
            <li>
                <?php print textfield($form['submitted'], 'subject'); ?>
            </li>
        </ul>
        <ul>
            <li>
                <?php print textarea($form['submitted'], 'your_question'); ?>
            </li>
            <li><?php print render($form['actions']['submit']); ?></li>
        </ul>
    </div>
    <div class="followus">
    </div>
</div>
<?php print drupal_render_children($form); ?>